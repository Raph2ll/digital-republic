# Boas vindas ao repositório do desafio da Digital Republic

Esse projeto foi desenvolvido para uma vaga de fullstack na empresa [Digital Republic](https://www.digitalrepublic.com.br)

A proposta do desafio é desenvolver uma tela que calcule a quantidade de tinta necessária para pintar uma sala. Com base na quantidade necessária o sistema deve projetar a quantidade de tinta que o usuário deve utilizar para pintar uma determinada área, sempre priorizando as latas maiores. Ex: se o usuário de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L.

# Sumário

- [Instruções](#instruções)
- [Stacks utilizadas](#stacks-utilizadas)
- [Documentação da API](#documentação-da-API)
- [Testes](#testes)
- [Screenshots](#screenshots)
- [Próximos passos](#próximos-passos)

## Instruções

Clonando o repositorio

```bash
  git@gitlab.com:Raph2ll/digital-republic.git
```

Entrando no repositorio

```bash
  cd digita-republic
```

Instale as dependencias e rode a aplicação

```bash
  npm i && npm run buildAll
```

Inicie a aplicação

```bash
  npm start
```

## Stacks utilizadas

**Front-end:** React.js, JavaScript, React-router-dom, Axios, ContextAPi, EsLint

O framework da aplicação é o React.js, para compartilha os estados entre os componentes foi usado Context e o Axios para consumir a API.

**Back-end:** Node.js, JavaScript, Express, Jest, SuperTest, Dotenv, EsLint

No Backend a aplicação foi desenvolvida utilizando Node.js/Express e JavaScript para estruturas as rotas.

## Documentação da API

#### Realiza o calculo das paredes

```http
  POST /

```

| Parâmetro   | Descrição  
| :---------- | :---------
| body | Não precisa de `chave` 

```json
{
		"walls": [
				{
					"height": 2,
					"width": 2,
					"windows": 0,
					"doors": 0
				},
				{
					"height": 3,
					"width": 2,
					"windows": 0,
					"doors": 0
				},
				{
					"height": 2,
					"width": 2,
					"windows": 0,
					"doors": 0
				},
				{
					"height": 2,
					"width": 2,
					"windows": 0,
					"doors": 0
				}
		]
}
```

### Resultado

```json
{
	"18": 0,
	"3.6": 1,
	"2.5": 0,
	"0.5": 0,
	"area": "3.60"
}
```
## Testes

### Backend

<img  src="assets/test.png" />

## Screenshots

### Tela responsiva

<img  src="assets/screen.png" />
<img  src="assets/responsivescreen.png" />
<img  src="assets/responsivescreen2.png" />

## Próximos passos

* Docker Compose

* Implementação do **Swagger** para documentação da API

* Deploy no **Heroku** + PM2

* Mais Testes

* Implementar mais funções no **NavBar**

* Rota para calcular latas de tinta "direto"

* Implementar um modal de Instruções

* Token de acesso **JWT**

* Adicionar uma página de **LOGIN/REGISTER**

* Implementar um CRUD

* Fazer a aplicação em TypeScript 
