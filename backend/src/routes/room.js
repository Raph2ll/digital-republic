const express = require('express');

const root = express.Router({ mergeParams: true });

root.post('/', require('../controller/room/room'));

module.exports = root;
