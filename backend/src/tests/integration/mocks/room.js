const manyWindowsAndDoors = {
  walls: [
    {
      height: 3,
      width: 3,
      windows: 5,
      doors: 5,
    },
  ],
};
const wallsNeedTo = {
  walls: [
    {
      height: 2,
      width: 4,
      windows: 1,
      doors: 1,
    },
  ],
};
const anyWall = {
  walls: [
    {
      height: 1,
      width: 1,
      windows: 1,
      doors: 1,
    },
  ],
};
const essencialField = {
  walls: [
    {
      height: 1,
      width: 0,
      windows: 1,
      doors: 1,
    },
  ],
};

const leftoverInk = {
  walls: [
    {
      height: 5,
      width: 3,
      windows: 1,
      doors: 1,
    },
  ],
};
const rightAnswer = {
  walls: [
    {
      height: 2,
      width: 2,
      windows: 0,
      doors: 0,
    },
    {
      height: 3,
      width: 2,
      windows: 0,
      doors: 0,
    },
    {
      height: 2,
      width: 2,
      windows: 0,
      doors: 0,
    },
    {
      height: 2,
      width: 2,
      windows: 0,
      doors: 0,
    },
  ],
};
module.exports = {
  manyWindowsAndDoors,
  wallsNeedTo,
  anyWall,
  essencialField,
  leftoverInk,
  rightAnswer,
};
