const request = require('supertest');
const {
  expect, it, describe,
} = require('@jest/globals');
const app = require('../../api/app');

const {
  manyWindowsAndDoors,
  wallsNeedTo,
  anyWall,
  essencialField,
  leftoverInk,
  rightAnswer,
} = require('./mocks/room');

describe('POST /', () => {
  describe(('Validation'), () => {
    it('Right Answer', async () => {
      const response = await request(app)
        .post('/')
        .send(rightAnswer);

      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual({
        18: 0,
        3.6: 1,
        2.5: 0,
        0.5: 0,
        area: '3.60',
      });
    });
    it('Json are required', async () => {
      const response = await request(app)
        .post('/')
        .send({});

      expect(response.statusCode).toBe(422);
      expect(response.body).toEqual({
        message: 'Fields are required',
      });
    });
    it('When you forget to fill in an essential field', async () => {
      const response = await request(app)
        .post('/')
        .send(essencialField);

      expect(response.statusCode).toBe(422);
      expect(response.body).toEqual({
        message: 'Fields are required',
      });
    });
    it('There should be no lack of ink', async () => {
      const response = await request(app)
        .post('/')
        .send(leftoverInk);

      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual({
        18.0: 0,
        3.6: 0,
        2.5: 1,
        0.5: 0,
        area: '2.22',
      });
    });
  });
  describe(('Business rules'), () => {
    it('manyWindowsAndDoors', async () => {
      const response = await request(app)
        .post('/')
        .send(manyWindowsAndDoors);

      expect(response.statusCode).toBe(422);
      expect(response.body).toEqual({
        message: 'Many windows and doors, the area of the windows and doors is more than 50% of the area of the walls',
      });
    });
    it('wallsNeedTo', async () => {
      const response = await request(app)
        .post('/')
        .send(wallsNeedTo);

      expect(response.statusCode).toBe(422);
      expect(response.body).toEqual({
        message: 'Walls with doors need to be 30 centimeters higher than the doors',
      });
    });
    it('anyWall', async () => {
      const response = await request(app)
        .post('/')
        .send(anyWall);

      expect(response.statusCode).toBe(422);
      expect(response.body).toEqual({
        message: 'Any wall must be more than 1m² and be less than or equal to 50m²',
      });
    });
  });
});
