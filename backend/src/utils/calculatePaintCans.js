function calculatePaintCans(area) {
  let toLitres = (area / 5).toFixed(2);
  const paintCans = [18.0, 3.6, 2.5, 0.5];

  const result = {
    18.0: 0, 3.6: 0, 2.5: 0, 0.5: 0, area: toLitres,
  };

  for (const cans of paintCans) {
    while (toLitres >= cans) {
      result[cans] += 1;
      toLitres -= cans;
    }
  }
  // because you can't run out of ink
  if (toLitres >= 0.1) result[0.5] += 1;
  // because 0.5:5 = 2.5:1
  if (result[0.5] >= 5) { result[0.5] = 0; result[2.5] += 1; }

  return result;
}

module.exports = calculatePaintCans;

// Reference https://pt.stackoverflow.com/questions/398871/calcular-troco-e-exibir-notas-disponíveis-com-javascript
