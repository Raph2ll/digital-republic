const manyWindowsAndDoors = {
  error: {
    message: 'Many windows and doors, the area of the windows and doors is more than 50% of the area of the walls',
  },
};
const areRequired = {
  error: {
    message: 'Fields are required',
  },
};
const wallsNeedTo = {
  error: {
    message: 'Walls with doors need to be 30 centimeters higher than the doors',
  },
};
const areaRequired = {
  error: {
    message: 'Area is required',
  },
};
const mustBeNumber = {
  error: {
    message: 'Area must be a number',
  },
};
const anyWall = {
  error: {
    message: 'Any wall must be more than 1m² and be less than or equal to 50m²',
  },
};

module.exports = {
  manyWindowsAndDoors,
  areRequired,
  wallsNeedTo,
  areaRequired,
  mustBeNumber,
  anyWall,
};
