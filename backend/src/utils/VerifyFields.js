const {
  areRequired, wallsNeedTo, areaRequired, areaBeNumber, anyWall,
} = require('./errors');

function wallFields({
  height, width, doors, getWalls,
}) {
  if (!height || !width) {
    return areRequired;
  }
  if (getWalls <= 1 || getWalls > 50) {
    return anyWall;
  }
  if (doors > 0 && height < 2.20) {
    return wallsNeedTo;
  }
  return false;
}

function areaFields(area) {
  if (!area) {
    return areaRequired;
  }
  if (typeof area !== 'number') {
    return areaBeNumber;
  }
  return false;
}

module.exports = { wallFields, areaFields };
