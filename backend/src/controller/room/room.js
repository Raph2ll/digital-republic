const { StatusCodes: { OK, UNPROCESSABLE_ENTITY } } = require('http-status-codes');
const service = require('../../services/necessaryInk');

const room = async (req, res, next) => {
  try {
    const { walls } = req.body;

    const necessaryInk = service(walls);

    if (necessaryInk.error) {
      return res.status(UNPROCESSABLE_ENTITY).json(necessaryInk.error);
    }

    return res.status(OK).json(necessaryInk);
  } catch (e) {
    console.log(e);
    next(e);
  }
};

module.exports = room;
