const { manyWindowsAndDoors, areaRequired, areRequired } = require('../utils/errors');
const calculatePaintCans = require('../utils/calculatePaintCans');
const { areaFields, wallFields } = require('../utils/VerifyFields');

function necessaryInk(room) {
  let area = 0;
  let windownsAndDoors = 0;
  let verifyWall = '';
  if (!room) return areRequired;
  room.forEach((wall) => {
    const {
      height, width, doors, windows,
    } = wall;

    const getWalls = width * height;
    const getWindows = windows * 2.0 * 1.20;
    const getDoors = doors * 0.80 * 1.90;

    // wallFields
    if (wallFields({
      height, width, doors, getWalls,
    })) {
      verifyWall = wallFields({
        height, width, doors, getWalls,
      });
      return;
    }
    windownsAndDoors += getWindows + getDoors;
    area += getWalls;
  });

  if (verifyWall) return verifyWall;

  if (windownsAndDoors > area / 2) {
    return manyWindowsAndDoors;
  }

  area -= windownsAndDoors;

  // areaFields
  if (areaFields(area)) {
    return areaFields(area);
  }
  return calculatePaintCans(area);
}

module.exports = necessaryInk;
