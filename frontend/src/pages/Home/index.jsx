import React from 'react';
import Navbar from '../../components/NavBar';
import CardContainer from '../../components/CardContainer';

import './style.css';

function Home() {
  return (
    <div className="body">
      <Navbar />
      <CardContainer />
    </div>
  );
}

export default Home;
