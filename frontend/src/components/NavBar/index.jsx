import React from 'react';
import './style.css';

function NavBar() {
  return (
    <nav className="navbar">
      <div className="container">
        <div className="navbar-header">
          <h1>Paint Calculator</h1>
        </div>
      </div>
    </nav>
  );
}
export default NavBar;
