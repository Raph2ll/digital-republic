import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

function Card({ setWallMeasurements, wallName }) {
  const [height, setHeight] = useState('');
  const [width, setWidth] = useState('');
  const [windows, setWindows] = useState('');
  const [doors, setDoors] = useState('');

  useEffect(() => {
    setWallMeasurements({
      height, width, doors, windows,
    });
  }, [height, width, doors, windows]);

  return (
    <div className="card">
      <h1>{ wallName }</h1>
      <div className="main-container">
        <label htmlFor="nHeight">
          Height
          <input
            type="number"
            name="nHeight"
            id="nHeight"
            onChange={({ target }) => setHeight(target.value)}
          />
        </label>
        <label htmlFor="nWidth">
          Width
          <input
            type="number"
            name="nWidth"
            id="nWidth"
            onChange={({ target }) => setWidth(target.value)}
          />
        </label>
        <label htmlFor="nWindows">
          Windows
          <input
            type="number"
            name="nWindows"
            id="nWindows"
            onChange={({ target }) => setWindows(target.value)}
          />
        </label>
        <label htmlFor="nDoors">
          Doors
          <input
            type="number"
            name="nDoors"
            id="nDoors"
            onChange={({ target }) => setDoors(target.value)}
          />
        </label>
      </div>
    </div>
  );
}

Card.propTypes = {
  wallName: PropTypes.string,
}.isRequired;

export default Card;
