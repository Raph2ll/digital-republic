import React, { useContext } from 'react';
import { MyContext } from '../../context';
import Card from '../Card';
import './style.css';

function CardContainer() {
  const {
    setFirstWall,
    setSecondWall,
    setThirdWall,
    setFourthWall,
    onSubmit,
    result,
  } = useContext(MyContext);

  return (
    <form onSubmit={onSubmit}>
      <div className="card-container">
        <Card setWallMeasurements={setFirstWall} wallName="Wall 1" />
        <Card setWallMeasurements={setSecondWall} wallName="Wall 2" />
        <Card setWallMeasurements={setThirdWall} wallName="Wall 3" />
        <Card setWallMeasurements={setFourthWall} wallName="Wall 4" />
      </div>
      <div className="result-container">
        <p className="result">{result}</p>
        <label htmlFor="nbutton">
          <input type="submit" value="Generate" />
        </label>
      </div>
    </form>
  );
}

export default CardContainer;
