import axios from 'axios';

const url = 'http://localhost:3001/';

async function room(walls) {
  try {
    const fetchApi = await axios.post(url, { walls });
    return fetchApi.data;
  } catch (error) {
    return { error };
  }
}

export default room;
