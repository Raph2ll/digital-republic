import React, {
  createContext, useState, useMemo, useEffect,
} from 'react';
import PropTypes from 'prop-types';
import toString from '../utils/toString';
import room from '../api/room';

export const MyContext = createContext();

function ContextProvider({ children }) {
  const [firstWall, setFirstWall] = useState({});
  const [secondWall, setSecondWall] = useState({});
  const [thirdWall, setThirdWall] = useState({});
  const [fourthWall, setFourthWall] = useState({});
  const [walls, setWalls] = useState([]);
  const [result, setResult] = useState('');

  useEffect(() => {
    setWalls([firstWall, secondWall, thirdWall, fourthWall]);
  }, [firstWall, secondWall, thirdWall, fourthWall]);

  const onSubmit = async (event) => {
    event.preventDefault();
    setWalls([firstWall, secondWall, thirdWall, fourthWall]);
    const apiPost = await room(walls);
    if (apiPost.error) {
      setResult(apiPost.error.response.data.message);
    } else {
      setResult(`This is the amount of paint you will need and the total area to be painted: ${toString(apiPost)}`);
    }
  };

  const context = {
    walls,
    setWalls,
    firstWall,
    setFirstWall,
    secondWall,
    setSecondWall,
    thirdWall,
    setThirdWall,
    fourthWall,
    setFourthWall,
    onSubmit,
    result,
  };

  const contextValue = useMemo(() => (context), [context]);

  return (
    <MyContext.Provider value={contextValue}>
      {children}
    </MyContext.Provider>
  );
}

ContextProvider.propTypes = {
  children: PropTypes.object,
}.isRequired;

export default ContextProvider;
