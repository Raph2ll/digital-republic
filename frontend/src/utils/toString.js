function toString(obj) {
  const objString = JSON.stringify(obj);
  const finalString = objString.replace(/[{}"]/g, '').split(',').join(', ');
  return finalString;
}

module.exports = toString;
